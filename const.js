import packageInfo from './package.json';

const { version } = packageInfo;

export const APP_VERSION = version;

export const telegramChannel = 'https://t.me/joinchat/iSi80Y2tmMcxZjdi';

export const locales = [
  // {
  //   id: 1,
  //   slug: 'ru',
  //   title: 'Russian',
  //   short: 'RU',
  // },
  // {
  //   id: 2,
  //   slug: 'en',
  //   title: 'English',
  //   short: 'EN',
  // },
  // {
  //   id: 3,
  //   slug: 'de',
  //   title: 'German',
  //   short: 'DE',
  // },
  {
    id: 4,
    slug: 'ua',
    title: 'Українська',
    short: 'UA',
  },
];

export const baseUrl = process.env.NEXT_PUBLIC_DOMAIN;
export const dashboardUrl = process.env.NEXT_PUBLIC_DASHBOARD_URL; // export const dashboardUrl = `https://dashboard.${baseUrl}/utils`;

export const api = {
  signUp: `${process.env.NEXT_PUBLIC_API_URL}/v2/users/sign-up`, // `https://api.${baseUrl}/users/sign-up`,
  login: `${process.env.NEXT_PUBLIC_API_URL}/auth/login`, // `https://api.${baseUrl}/auth/login`,
  contactUs: `${process.env.NEXT_PUBLIC_API_URL}/contacts/contact-us`, // `https://api.${baseUrl}/contacts/contact-us`,
  personalCall: `${process.env.NEXT_PUBLIC_API_URL}/contacts/personal-call`, // `https://api.${baseUrl}/contacts/personal-call`,
  guideRequest: `${process.env.NEXT_PUBLIC_API_URL}/contacts/guide-request`, // `https://api.${baseUrl}/contacts/guide-request`,
  callingCodes: `${process.env.NEXT_PUBLIC_API_URL}/dicts/codes`, // `https://api.${baseUrl}/calling-codes`,
};

export const ipInfoService = {
  url: 'https://ipinfo.io',
  token: '1ccc0bf4a4db2a',
};

export const isStage = !!baseUrl?.match(/devstage.cloud/);
