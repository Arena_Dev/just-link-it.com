import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { store } from '../store/store';
import { Provider } from 'react-redux';
import '../styles/globals.scss';
import { translator } from 'translator';

import { MetaRobots } from 'components/shared/MetaRobots';

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const { locale } = router;

  if (translator.language !== locale) {
    translator.changeLanguage(locale);
  }

  useEffect(() => {
    translator.changeLanguage(locale);
  }, [locale]);

  return (
    <Provider store={store}>
      <MetaRobots />
      <Component {...pageProps} key={`page-${locale}`} />
    </Provider>
  );
}

export default MyApp;
