import { Page } from '../components/layout/Page';
import { SectionPromo } from 'components/pages/homepage/SectionPromo';
import { SectionFindYourself } from 'components/pages/homepage/SectionFindYourself';
import { SectionRequirements } from 'components/pages/homepage/SectionRequirements';
import { SectionTasks } from 'components/pages/homepage/SectionTasks';
import { SectionInvest } from 'components/pages/homepage/SectionInvest';
import { SectionCourse } from 'components/pages/homepage/SectionCourse';
import { SectionWorkWithUs } from 'components/pages/homepage/SectionWorkWithUs';

import content from 'content';
import { SectionHomepageSignUp } from 'components/pages/homepage/SectionHomepageSignUp';

const { homepage } = content;

export default function Home() {
  const { meta } = homepage();

  return (
    <Page {...meta}>
      <SectionPromo />
      <SectionFindYourself />
      <SectionRequirements />
      <SectionTasks />
      <SectionInvest />
      <SectionCourse />
      <SectionWorkWithUs />
      <SectionHomepageSignUp />
    </Page>
  );
}
