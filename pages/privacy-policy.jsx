import { Page } from 'components/layout/Page';
import { SectionLegalContent } from 'components/pages/legal/SectionLegalContent';
import { SectionLegalPromo } from 'components/pages/legal/SectionLegalPromo';

import content from 'content';

const { policyPage } = content;

const Policy = () => {
  const { meta, heading, releaseDate, items } = policyPage();

  return (
    <Page {...meta}>
      <SectionLegalPromo
        heading={heading}
        releaseDate={releaseDate}
        image={{
          src: '/img/pages/privacy/privacy-promo.png',
          src2x: '/img/pages/privacy/privacy-promo@2x.png',
          width: 74,
          height: 105,
        }}
      />
      <SectionLegalContent items={items} />
    </Page>
  );
};

export default Policy;
