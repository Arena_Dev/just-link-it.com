import { Page } from 'components/layout/Page';
import content from 'content';
import { SectionCollaborationPromo } from 'components/pages/cooperation/SectionCollaborationPromo';
import { SectionCollaborationContent } from 'components/pages/cooperation/SectionCollaborationContent';
import { SectionCollaborationSignUp } from 'components/pages/cooperation/SectionCollaborationSignUp';

const { cooperation } = content;

const Cooperation = () => {
  const { meta, heading, releaseDate } = cooperation();

  return (
    <Page {...meta}>
      <SectionCollaborationPromo
        heading={heading}
        releaseDate={releaseDate}
        image={{
          src: '/img/pages/collaboration/hero.png',
          src2x: '/img/pages/collaboration/hero@2x.png',
          width: 612,
          height: 310,
        }}
      />
      <SectionCollaborationContent />
      <SectionCollaborationSignUp />
    </Page>
  );
};

export default Cooperation;
