import { Page } from 'components/layout/Page';

import content from 'content';
import { SectionAboutUsPromo } from 'components/pages/about-us/SectionAboutUsPromo';
import { SectionAboutUsAbout } from 'components/pages/about-us/SectionAboutUsAbout';
import { SectionAboutUsItems } from 'components/pages/about-us/SectionAboutUsItems';
import { SectionAboutUsSignUp } from 'components/pages/about-us/SectionAboutUsSignUp';

const { aboutUsPage } = content;

const AboutUs = () => {
  const { meta } = aboutUsPage();

  return (
    <Page {...meta}>
      <SectionAboutUsPromo />
      <SectionAboutUsAbout />
      <SectionAboutUsItems />
      <SectionAboutUsSignUp />
    </Page>
  );
};

export default AboutUs;
