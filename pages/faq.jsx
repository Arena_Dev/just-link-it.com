import { Page } from 'components/layout/Page';
import { SectionFaqContent } from 'components/pages/faq/SectionFaqContent';
import SectionPromo from 'components/pages/faq/SectionPromo';

import content from 'content';

const { faqPage } = content;

const Faq = () => {
  const { meta, heading, faq } = faqPage();

  return (
    <Page {...meta}>
      <SectionPromo heading={heading} />
      <SectionFaqContent faq={faq} />
    </Page>
  );
};

export default Faq;
