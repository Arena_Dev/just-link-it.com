import { Page } from 'components/layout/Page';
import { SectionContactsPromo } from 'components/pages/contacts/SectionContactsPromo';
import { SectionGetCall } from 'components/pages/contacts/SectionGetCall';

import content from 'content';

const { contactsPage } = content;

const Contacts = () => {
  const { meta } = contactsPage();

  return (
    <Page {...meta}>
      <SectionContactsPromo />
      <SectionGetCall />
    </Page>
  );
};

export default Contacts;
