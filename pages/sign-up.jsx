import { Page } from 'components/layout/Page';
import { SectionSignUpPromo } from 'components/pages/sign-up/SectionSignUpPromo';
import content from 'content';

const { signUpPage } = content;

const SignUp = () => {
  const { meta } = signUpPage();

  return (
    <Page {...meta}>
      <SectionSignUpPromo />
    </Page>
  );
};

export default SignUp;
