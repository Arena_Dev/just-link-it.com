import { Page } from 'components/layout/Page';
import { SectionLegalContent } from 'components/pages/legal/SectionLegalContent';
import { SectionLegalPromo } from 'components/pages/legal/SectionLegalPromo';

import content from 'content';

const { termsPage } = content;

const Terms = () => {
  const { meta, heading, items, releaseDate } = termsPage();

  return (
    <Page {...meta}>
      <SectionLegalPromo
        heading={heading}
        releaseDate={releaseDate}
        image={{
          src: '/img/pages/terms/terms-promo.png',
          src2x: '/img/pages/terms/terms-promo@2x.png',
          width: 64,
          height: 98,
        }}
      />
      <SectionLegalContent items={items} />
    </Page>
  );
};

export default Terms;
