import classNames from 'classnames';
import styles from './UnorderedList.module.scss';

export const UnorderedList = ({ children, headings, dots = true, markerColor }) => {
  return (
    <ul
      className={classNames(
        styles.list,
        headings && styles.headings,
        dots ? styles.dots : styles.headingsAccent,
        styles[markerColor]
      )}
    >
      {children}
    </ul>
  );
};
