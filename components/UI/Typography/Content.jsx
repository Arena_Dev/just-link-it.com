import classNames from 'classnames';
import styles from './Content.module.scss';

export const Content = ({ className, children }) => {
  return <div className={classNames(styles.content, className)}>{children}</div>;
};
