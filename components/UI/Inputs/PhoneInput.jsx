import axios from 'axios';
import classNames from 'classnames';
import { useEffect, useState } from 'react';
import Input from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import styles from './PhoneInput.module.scss';

import { ipInfoService } from 'const';

export const PhoneInput = ({
  value,
  placeholderText,
  validate,
  className,
  country = 'us',
  onChange,
  onBlur,
  onFocus,
  onClick,
  onKeyDown,
  detectLocation = false,
  ...rest
}) => {
  const [validation, setValidation] = useState({
    isValid: true,
    message: null,
  });
  const [detectedCountry, setDetectedCountry] = useState('');

  const classes = [styles.inputWrapper, !validation.isValid && styles.invalid, className];

  const handleBlur = (e, data) => {
    let result;

    if (validate) {
      result = validate(e.target.value.replace(/\D/, ''));
      setValidation(result);
    }

    if (onBlur) onBlur(e, data, result?.isValid);
  };

  const handleChange = (value, data, e, formattedValue) => {
    let result;

    if (validate) {
      result = validate(value);
      if (result.isValid) setValidation(result);
    }

    if (onChange) onChange(value, data, e, formattedValue, result?.isValid);
  };

  useEffect(() => {
    if (detectLocation === true) {
      const savedCountry = localStorage.getItem('country');

      if (savedCountry) {
        setDetectedCountry(savedCountry);
      } else {
        axios
          .get(ipInfoService.url, {
            headers: {
              Accept: 'application/json',
              authorization: `Bearer ${ipInfoService.token}`,
            },
          })
          .then(({ data }) => {
            const resultCountry = data.country.toLowerCase();
            setDetectedCountry(resultCountry);
            localStorage.setItem('country', resultCountry);
          })
          .catch((e) => {
            setDetectedCountry(country);
          });
      }
    }
  }, [detectLocation, country]);

  return (
    <Input
      containerClass={classNames(classes)}
      country={detectedCountry}
      disableInitialCountryGuess={false}
      disableCountryGuess={false}
      value={value}
      placeholder={placeholderText}
      onChange={handleChange}
      onBlur={handleBlur}
      onFocus={onFocus}
      onClick={onClick}
      onKeyDown={onKeyDown}
      inputProps={rest}
      excludeCountries={['ru']}
    />
  );
};
