import classNames from 'classnames';
import { useState } from 'react';
import styles from './LabelInput.module.scss';

import EyeOpen from 'public/img/icon-pack/eye.svg';
import EyeHidden from 'public/img/icon-pack/eye-disabled.svg';
import { _ } from 'translator';

export const LabelInput = ({
  className,
  inputId,
  placeholderText,
  validate: userValidate, // return object {isValid: bool, message: str}
  isRequired,
  inputRef,
  children,
  icon,
  type = 'text',
  onChange,
  onBlur,
  ...rest
}) => {
  const [validation, setValidation] = useState({
    isValid: true,
    message: null,
  });

  const [hidden, setHidden] = useState(type === 'password');

  const classes = [
    styles.label,
    styles[icon],
    type === 'password' && styles.password,
    isRequired && styles.required,
    !validation.isValid && styles.invalid,
    className,
  ];

  const onHideClick = () => {
    setHidden(!hidden);
  };

  const validate = (value) => {
    let validation = { isValid: true };

    if (userValidate) {
      validation = userValidate(value);
    }

    if (isRequired && value === '' && validation.isValid) {
      validation = { isValid: false, message: _('fields.required') };
    }

    return validation;
  };

  const handleBlur = (e) => {
    let validation = validate(e.target.value);
    setValidation(validation);

    if (onBlur) onBlur(e, validation?.isValid);
  };

  const handleChange = (e) => {
    let validation = validate(e.target.value);

    if (validation.isValid) setValidation(validation);

    if (onChange) onChange(e, validation?.isValid);
  };

  return (
    <label htmlFor={inputId} className={classNames(classes)}>
      {children && <span className={styles.labelText}>{children}</span>}
      {type === 'textarea' ? (
        <textarea
          ref={inputRef}
          id={inputId}
          placeholder={placeholderText}
          className={styles.textarea}
          type={type}
          onChange={handleChange}
          onBlur={handleBlur}
          {...rest}
        ></textarea>
      ) : (
        <span className={styles.inputWrapper}>
          <input
            ref={inputRef}
            id={inputId}
            placeholder={placeholderText}
            className={styles.input}
            type={hidden ? 'password' : type !== 'password' ? type : 'text'}
            onChange={handleChange}
            onBlur={handleBlur}
            {...rest}
          />
          {type === 'password' && (
            <span className={styles.eye} onClick={onHideClick}>
              {hidden ? <EyeHidden /> : <EyeOpen />}
            </span>
          )}
        </span>
      )}
      <span className={styles.validationText}>{validation.message}</span>
    </label>
  );
};
