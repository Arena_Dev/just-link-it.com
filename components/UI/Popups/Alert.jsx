import classNames from 'classnames';
import styles from './Alert.module.scss';

export const Alert = ({ type, active, children, className }) => {
  return (
    <div className={classNames(styles.alert, styles[type], active && styles.active, className)}>
      {children}
    </div>
  );
};
