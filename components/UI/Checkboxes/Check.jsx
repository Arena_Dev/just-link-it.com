import { useState } from 'react';
import classNames from 'classnames';
import styles from './Check.module.scss';

const Check = ({ onChange, checked, className, ...rest }) => {
  const [check, setCheck] = useState(!!checked);
  const [focus, setFocus] = useState(false); // FIXME: not working yet

  const handleChange = (e) => {
    setCheck(e.target.checked);

    if (onChange) onChange(e);
  };

  const onFocus = () => {
    setFocus(true);
  };

  const onBlur = () => {
    setFocus(false);
  };

  const classes = [styles.checkbox, check && styles.checked, focus && styles.focus, className];

  return (
    <label className={classNames(classes)}>
      <input
        type="checkbox"
        checked={check}
        onChange={handleChange}
        onFocus={onFocus}
        onBlur={onBlur}
        {...rest}
      />
    </label>
  );
};

export default Check;
