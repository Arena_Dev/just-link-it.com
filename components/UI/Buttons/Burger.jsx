import classNames from 'classnames'
import styles from './Burger.module.scss'

export const Burger = ({expand, toggle, className}) => {
  return (
    <div className={classNames(styles.hamburger, expand && styles.active, className)} onClick={toggle}>
      <div className={styles.box}>
        <div className={styles.inner}></div>
      </div>
    </div>
  )
}
