import PropTypes from 'prop-types';
import classNames from 'classnames';
import Link from 'next/link';
import styles from './Button.module.scss';

export const Button = ({
  type,
  size,
  className,
  href,
  disabled,
  children,
  externalLink,
  htmlType,
  ...restProps
}) => {
  const props = {
    className: classNames(
      styles.btn,
      styles[type],
      styles[size],
      disabled && styles.disabled,
      className
    ),
    ...restProps,
  };

  const render = () => {
    const isLink = href && !disabled;
    if (isLink && !externalLink) {
      return (
        <Link href={href}>
          <a {...props}>{children}</a>
        </Link>
      );
    } else if (!!externalLink) {
      return (
        <a href={href} {...props}>
          {children}
        </a>
      );
    } else {
      return (
        <button type={htmlType} {...props}>
          {children}
        </button>
      );
    }
  };

  return render();
};

Button.propTypes = {
  size: PropTypes.string,
  type: PropTypes.string,
  href: PropTypes.string,
  disabled: PropTypes.bool,
  externalLink: PropTypes.bool,
};
