import classNames from 'classnames';
import { PaymentMethods } from 'components/shared/PaymentMethods';
import { Container } from 'components/layout/Container';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './Footer.module.scss';

import { _ } from 'translator';
import { locales } from 'const';

export const Footer = () => {
  const router = useRouter();
  return (
    <footer>
      <div className={styles.content}>
        <Container className={styles.contentWrapper}>
          <div className={styles.footerMenu}>
            <ul>
              <li>
                <Link href="/faq">
                  <a>{_('nav.faq')}</a>
                </Link>
              </li>
              <li>
                <Link href="/about-us">
                  <a>{_('nav.aboutUs')}</a>
                </Link>
              </li>
              <li>
                <Link href="/contact-us">
                  <a>{_('nav.contact')}</a>
                </Link>
              </li>
              <li>
                <Link href="/cooperation">
                  <a>{_('nav.cooperation')}</a>
                </Link>
              </li>
              <li>
                <Link href="/privacy-policy">
                  <a>{_('nav.privacyPolicy')}</a>
                </Link>
              </li>
              <li>
                <Link href="/terms-and-conditions">
                  <a>{_('nav.termsAndConditions')}</a>
                </Link>
              </li>
            </ul>
          </div>
          <div className={styles.contacts}>
            <ul>
              <li>
                <a href="https://t.me/HRJustLinkIt" target="_blank" rel="noreferrer noopener">
                  <i className={classNames(styles.icon, 'icon-telegram')}></i>
                  @HRJustLinkIt
                </a>
              </li>
              <li>
                <a href="mailto:support@just-link-it.com" rel="noreferrer noopener">
                  <i className={classNames(styles.icon, 'icon-headphones')}></i>
                  support@just-link-it.com
                </a>
              </li>
            </ul>
            {locales.length > 1 && (
              <div className={styles.locales}>
                {locales.map((item) => (
                  <Link key={item.slug} href={router.pathname} locale={item.slug}>
                    <a className={classNames(router.locale == item.slug && styles.active)}>
                      {item.short}
                    </a>
                  </Link>
                ))}
              </div>
            )}
          </div>
          <p className={styles.text}>{_('footer.about')}</p>
        </Container>
      </div>
      <div className={styles.copyright}>
        <Container className={styles.copyWrapper}>
          <PaymentMethods />
          <p>
            &copy; Just-Link-It {new Date().getFullYear()}. {_('footer.allRightsReserved')}
          </p>
        </Container>
      </div>
    </footer>
  );
};
