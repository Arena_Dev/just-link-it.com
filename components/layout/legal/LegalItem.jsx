import classNames from 'classnames';
import { Button } from 'components/UI/Buttons/Button';
import { UnorderedList } from 'components/UI/Typography/UnorderedList';
import { debounce } from 'lodash';
import { useEffect, useRef, useState } from 'react';
import JsxParser from 'react-jsx-parser';
import styles from './LegalItem.module.scss';
import Link from 'next/link';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

export const LegalItem = ({ itemId, heading, content, image }) => {
  const contentWrapper = useRef(null);
  const [open, setOpen] = useState(false);
  const openRef = useRef(open);

  openRef.current = open;

  const components = { UnorderedList, Button, Link };

  const toggleItem = () => {
    if (window.matchMedia('(max-width: 576px)').matches) {
      if (open) {
        contentWrapper.current.style.maxHeight = 0;
      } else {
        contentWrapper.current.style.maxHeight = contentWrapper.current.scrollHeight + 'px';
      }
    } else {
      contentWrapper.current.style.maxHeight = 'none';
    }

    setOpen(!open);
  };

  const checkResize = debounce(() => {
    if (window.matchMedia('(max-width: 576px)').matches) {
      if (openRef.current) {
        contentWrapper.current.style.maxHeight = contentWrapper.current.scrollHeight + 'px';
      } else {
        contentWrapper.current.style.maxHeight = 0;
      }
    } else {
      contentWrapper.current.style.maxHeight = 'none';
    }
  }, 50);

  useEffect(() => {
    window.addEventListener('resize', checkResize);

    return () => {
      window.removeEventListener('resize', checkResize);
    };
  }, [checkResize]);

  return (
    <div
      className={classNames(styles.legalItem, {
        [styles.legalItemOpen]: open,
      })}
    >
      <div id={itemId} className={styles.anchor}></div>

      <div className={styles.headingWrap} onClick={toggleItem}>
        {image && <ResponsiveImage {...image} className={styles.image} />}
        <h2 className={styles.heading}>{heading}</h2>
        <i className={classNames(styles.icon, 'icon-chevron-down')}></i>
      </div>
      <div ref={contentWrapper} className={styles.contentWrapper}>
        <div className={styles.content}>
          <JsxParser className={styles.contentInner} jsx={content} components={components} />
        </div>
      </div>
    </div>
  );
};
