import classNames from 'classnames';
import { useEffect, useState, useRef, useCallback } from 'react';
import { throttle } from 'lodash';
import { gsap } from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import styles from './ContentsTable.module.scss';

import { _ } from 'translator';

gsap.registerPlugin(ScrollToPlugin);

export const ContentsTable = ({ items }) => {
  if (!items.length) return null;
  return <Core items={items} />;
};

const Core = ({ items }) => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [winWidth, setWinWidth] = useState(0);
  const [scrollingMode, setScrollingMode] = useState(true);

  const scroller = useRef(null);
  const navContainer = useRef(null);
  const scrollingModeRef = useRef(scrollingMode);
  const activeIndexRef = useRef(activeIndex);

  scrollingModeRef.current = scrollingMode;
  activeIndexRef.current = activeIndex;

  const checkPosition = useCallback(() => {
    let elemFound = false;
    const winTop = window.scrollY;

    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
      setActiveIndex(items.length - 1);
      return;
    }

    items.forEach((item, index) => {
      if (elemFound) return;
      const elem = document.getElementById(item.id);
      if (!elem) return;
      const targetElem = elem.parentNode;

      const elTop = winTop + targetElem.getBoundingClientRect().top;

      if (window.matchMedia('(max-width: 992px)').matches) {
        const targetHeight = winTop + 200;

        if (targetHeight >= elTop && winTop < elTop + targetElem.clientHeight - 200) {
          setActiveIndex(index);

          elemFound = true;
        }
      } else {
        const targetHeight = winTop + 200;
        const topDiff = 140;
        const bottomDiff = 80;

        if (
          targetHeight >= elTop - topDiff &&
          winTop < elTop + targetElem.clientHeight - bottomDiff
        ) {
          setActiveIndex(index);

          elemFound = true;
        }
      }
    });
  }, [items]);

  // const throttledCheckPosition = throttle(checkPosition, 100);
  const throttledCheckPosition = checkPosition;

  const checkTableScroll = useCallback(() => {
    if (!scrollingModeRef.current) return;
    const targetElem = navContainer.current?.querySelector(
      `[href="#${items[activeIndexRef.current]?.id}"]`
    );
    if (!targetElem) return;
    const parent = targetElem.parentNode;

    if (window.matchMedia('(max-width: 992px)').matches) {
      gsap.to(navContainer.current, 0.25, {
        scrollTo: {
          x:
            navContainer.current.scrollLeft +
            parent.getBoundingClientRect().left -
            30 -
            (navContainer.current.clientWidth / 2 - parent.clientWidth / 2 - 30),
          onComplete: () => {
            const leftPosition =
              navContainer.current.scrollLeft + parent.getBoundingClientRect().left;
            scroller.current.style.width = parent.clientWidth + 'px';
            scroller.current.style.transform = `translateX(${leftPosition}px)`;
          },
        },
      });
    } else {
      scroller.current.style.width = '4px';

      gsap.to(navContainer.current, 0.25, {
        scrollTo: {
          y:
            navContainer.current.scrollTop +
            (parent.getBoundingClientRect().top -
              navContainer.current.getBoundingClientRect().top) -
            navContainer.current.clientHeight / 2,
          onComplete: () => {
            const topPosition =
              navContainer.current.scrollTop +
              parent.getBoundingClientRect().top -
              navContainer.current.getBoundingClientRect().top +
              5;
            scroller.current.style.transform = `translateY(${topPosition}px)`;
          },
        },
      });
    }
  }, [items]);

  useEffect(() => {
    checkTableScroll();
  }, [activeIndex, winWidth, checkTableScroll]);

  const updateWindowWidth = throttle(() => {
    setWinWidth(window.innerWidth);
  }, 200);

  useEffect(() => {
    window.addEventListener('scroll', throttledCheckPosition);
    window.addEventListener('resize', throttledCheckPosition);
    window.addEventListener('resize', updateWindowWidth);
    checkPosition();

    return () => {
      window.removeEventListener('scroll', throttledCheckPosition);
      window.removeEventListener('resize', throttledCheckPosition);
      window.removeEventListener('resize', updateWindowWidth);
    };
  }, [checkPosition, throttledCheckPosition, updateWindowWidth]);

  const onItemClick = (event, id) => {
    event.preventDefault();

    const el = document.getElementById(id)?.parentNode;

    const offset = window.matchMedia('(max-width: 992px)').matches ? 155 : 70;

    setScrollingMode(false);

    gsap.to(window, 1, {
      scrollTo: {
        y: window.scrollY + el.getBoundingClientRect().top - offset,
      },
      onComplete: () => {
        setScrollingMode(true);
        checkTableScroll();
      },
    });
  };

  return (
    <div className={styles.contentsTable}>
      <p className={styles.heading}>{_('shared.tableOfContents')}</p>
      <div className={styles.navWrapper}>
        <nav className={styles.nav} ref={navContainer}>
          <span ref={scroller} className={styles.navScroller}></span>
          <ul className={styles.list}>
            {items.map((item, index) => (
              <li
                key={item.id}
                className={classNames(styles.item, {
                  [styles.itemActive]: index === activeIndex,
                })}
              >
                <a href={`#${item.id}`} onClick={(event) => onItemClick(event, item.id)}>
                  {item.heading}
                </a>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </div>
  );
};
