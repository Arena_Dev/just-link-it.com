import { useState, useEffect } from 'react';
import { Logo } from 'components/shared/Logo';
import { Container } from 'components/layout/Container';
import { ResponsiveMenu } from './ResponsiveMenu';
import { Burger } from 'components/UI/Buttons/Burger';
import { Button } from 'components/UI/Buttons/Button';
import { useEventListener } from 'helpers/useEventListener';
import classNames from 'classnames';
import styles from './Header.module.scss';

import { dashboardUrl } from 'const';
import { getMenu } from 'content';
import { _ } from 'translator';

const signInUrl = `${dashboardUrl}?ref_landing`;

export const Header = () => {
  const [isSticky, setSticky] = useState(false);
  const [isExpanded, setExpanded] = useState(false);

  const menuLinks = getMenu();

  const handleScroll = (args) => {
    setSticky(window.scrollY > 30);
  };

  const burgerClick = (e) => {
    setExpanded(!isExpanded);
  };

  useEventListener('scroll', handleScroll);

  useEffect(() => {
    document.body.classList[isExpanded ? 'add' : 'remove']('overflowHidden');
  }, [isExpanded]);

  return (
    <header className={classNames(styles.header, isSticky && styles.sticky)}>
      <Container className={styles.wrapper}>
        <Logo />
        <ResponsiveMenu links={menuLinks} signInUrl={signInUrl} mobileExpanded={isExpanded} />
        <div className={styles.right}>
          <div className={styles.auth}>
            <Button type="primaryOutline" className={styles.authBtn} href={signInUrl} externalLink>
              <i className="icon-user"></i>
              {_('buttons.signIn')}
            </Button>
            <Button type="primary" className={styles.authBtn} href="/sign-up">
              <i className="icon-user"></i>
              {_('buttons.signUp')}
            </Button>
          </div>
          <Burger expand={isExpanded} toggle={burgerClick} />
        </div>
      </Container>
    </header>
  );
};
