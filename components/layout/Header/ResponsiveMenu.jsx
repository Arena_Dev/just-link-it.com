import Link from 'next/link';
import { useRouter } from 'next/router';
import classNames from 'classnames';
import styles from './ResponsiveMenu.module.scss';

import { _ } from 'translator';
import { Button } from 'components/UI/Buttons/Button';

export const ResponsiveMenu = ({ links, signInUrl, mobileExpanded }) => {
  const router = useRouter();

  return (
    <div className={classNames(styles.menu, mobileExpanded && styles.expanded)}>
      <div className={styles.login}>
        <Button type="primaryOutline" className={styles.authBtn} href={signInUrl} externalLink>
          <i className="icon-user"></i>
          {_('buttons.signIn')}
        </Button>
        <Button type="primary" className={styles.authBtn} href="/sign-up">
          <i className="icon-user"></i>
          {_('buttons.signUp')}
        </Button>
      </div>
      <ul>
        {links.map((link) => (
          <li
            key={link.title}
            className={classNames(styles.link, router.pathname === link.href && styles.active)}
          >
            <Link href={link.href}>
              <a>{link.title}</a>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};
