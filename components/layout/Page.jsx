import Head from 'next/head';
import { useState } from 'react';
import { Alert } from 'components/UI/Popups/Alert';
import { Header } from './Header/Header';
import { Footer } from './Footer/Footer';
import styles from './Page.module.scss';

import { PageContext } from 'context/PageContext';

export const Page = ({ children, title = 'Just-Link-It', description = '' }) => {
  const [alert, setAlert] = useState({ text: '', type: '', active: false });

  const toggleAlert = ({ text, type = 'success', duration = 2000 }) => {
    setAlert({ text, type, active: true });

    setTimeout(() => {
      setAlert({ text, type, active: false });
    }, duration);
  };

  return (
    <PageContext.Provider value={{ toggleAlert }}>
      <Head>
        <title key="title">{title}</title>
        <meta key="description" name="description" content={description} />
        <meta key="og-title" property="og:title" content={title} />
        <meta key="og-description" property="og:description" content={description} />
        {/* <link rel="canonical" href={'https://' + baseUrl + currentPagePath} /> */}
      </Head>

      <div className={styles.pageWrap}>
        <Alert type={alert.type} active={alert.active}>
          {alert.text}
        </Alert>

        <Header />

        <div className={styles.page}>{children}</div>

        <Footer />
      </div>
    </PageContext.Provider>
  );
};
