import styles from './ContactForm.module.scss';
import { Button } from 'components/UI/Buttons/Button';
import { LabelInput } from 'components/UI/Inputs/LabelInput';

import { useContext, useState } from 'react';
import { PageContext } from 'context/PageContext';
import axios from 'axios';
import { api } from 'const';
import { _ } from 'translator';
import { validateName } from 'helpers/utils';

export const ContactForm = () => {
  const [email, setEmail] = useState({ value: '', valid: null });
  const [name, setName] = useState({ value: '', valid: null });
  const [message, setMessage] = useState({ value: '', valid: null });
  const [btnState, setBtnState] = useState({
    text: 'buttons.sendAnEmail',
    disabled: false,
  });

  const { toggleAlert } = useContext(PageContext);

  const submit = () => {
    setBtnState({ ...btnState, text: 'status.loading', disabled: true });
    axios
      .put(
        api.contactUs,
        {
          name: name.value,
          email: email.value,
          message: message.value,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then((response) => {
        setEmail({ value: '', valid: null });
        setName({ value: '', valid: null });
        setMessage({ value: '', valid: null });

        toggleButtonState('status.sent');
        toggleAlert({ text: _('status.messageSent'), type: 'success' });
      })
      .catch((response) => {
        toggleButtonState('status.error');
        toggleAlert({ text: _('status.somethingWrong'), type: 'error' });
      });
  };

  const btnClick = () => {
    if ([name, email, message].every((item) => item.valid)) submit();
  };

  const fields = [
    {
      name: 'name',
      label: _('fields.firstName.label'),
      value: name.value,
      isRequired: true,
      validate: validateName,
      onChange: (e, isValid) => {
        setName({ value: e.target.value, valid: isValid });
      },
    },
    {
      name: 'email',
      type: 'email',
      label: _('fields.email.label'),
      value: email.value,
      isRequired: true,
      validate: (value) => {
        return {
          isValid: !!value.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/),
          message: _('fields.email.validation'),
        };
      },
      onChange: (e, isValid) => {
        setEmail({ value: e.target.value, valid: isValid });
      },
    },
    {
      name: 'message',
      type: 'textarea',
      className: styles.textarea,
      label: _('fields.message.label'),
      value: message.value,
      isRequired: true,
      onChange: (e, isValid) => {
        setMessage({ value: e.target.value, valid: isValid });
      },
    },
  ];

  const toggleButtonState = (text) => {
    setBtnState({ ...btnState, text, disabled: true });

    setTimeout(() => {
      setBtnState({ ...btnState, text: 'buttons.sendAnEmail', disabled: false });
    }, 2000);
  };

  return (
    <div className={styles.form}>
      {fields.map((field) => (
        <LabelInput key={field.name} {...field}>
          {field.label}
        </LabelInput>
      ))}

      <Button type="primary" className={styles.btn} disabled={btnState.disabled} onClick={btnClick}>
        {_(btnState.text)}
      </Button>
    </div>
  );
};
