import { useState, useContext } from 'react';
import classNames from 'classnames';
import axios from 'axios';
import { Button } from 'components/UI/Buttons/Button';
import { PhoneInput } from 'components/UI/Inputs/PhoneInput';
import styles from './GetCallForm.module.scss';

import { PageContext } from 'context/PageContext';

import { _ } from 'translator';
import { api } from 'const';

const GetCallForm = ({ className }) => {
  const [phone, setPhone] = useState({ value: '', dialCode: '', valid: null });
  const [btnState, setBtnState] = useState({
    text: 'callBackForm.btnText',
    defaultText: 'callBackForm.btnText',
    disabled: false,
  });

  const { toggleAlert } = useContext(PageContext);

  const field = {
    name: 'phone',
    country: _('fields.phone.country'),
    detectLocation: true,
    placeholderText: _('fields.phone.placeholder'),
    value: phone.value,
    validate: (value) => {
      return {
        isValid: value.length > 7,
        message: _('fields.phone.validation'),
      };
    },
    onChange: (value, data, e, formattedValue, isValid) => {
      setPhone({
        value: value,
        dialCode: data.dialCode,
        valid: isValid,
      });
    },
  };

  const submit = () => {
    setBtnState({ ...btnState, text: 'status.loading', disabled: true });
    axios
      .put(
        api.personalCall,
        { phoneNumber: phone.value },
        {
          headers: { 'Content-Type': 'application/json' },
        }
      )
      .then((response) => {
        setPhone({ value: '', dialCode: '', valid: null });

        toggleButtonState('status.sent');
        toggleAlert({ text: _('status.requestSent'), type: 'success' });
      })
      .catch((response) => {
        toggleButtonState('status.error');
        toggleAlert({ text: _('status.somethingWrong'), type: 'error' });
      });
  };

  const btnClick = () => {
    if (phone.valid) submit();
  };

  const toggleButtonState = (text) => {
    setBtnState({ ...btnState, text, disabled: true });

    setTimeout(() => {
      setBtnState({ ...btnState, text: btnState.defaultText, disabled: false });
    }, 2000);
  };

  return (
    <div className={classNames(styles.form, className)}>
      <span className={styles.label}>{_('callBackForm.inputTitle')}</span>
      <PhoneInput {...field} />
      <Button
        type="primaryOutline"
        disabled={btnState.disabled}
        className={styles.btn}
        onClick={btnClick}
      >
        {_(btnState.text)}
      </Button>
    </div>
  );
};

export default GetCallForm;
