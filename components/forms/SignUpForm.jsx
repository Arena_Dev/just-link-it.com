import cn from 'classnames';
import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import styles from './SignUpForm.module.scss';

import { PageContext } from 'context/PageContext';

import { api, locales } from 'const';
import { useCookie } from 'helpers/useCookie';
import { _ } from 'translator';
import { PhoneInput } from 'components/UI/Inputs/PhoneInput';
import { LabelInput } from 'components/UI/Inputs/LabelInput';
import { Button } from 'components/UI/Buttons/Button';
import WelcomeBanner from 'components/banners/WelcomeBanner';
import { validateName } from 'helpers/utils';

export const SignUpForm = () => {
  const [referralCode, setReferralCode] = useState();
  const [email, setEmail] = useState({ value: '', valid: false });
  const [phone, setPhone] = useState({ value: '', dialCode: '', valid: false });
  const [firstName, setFirstName] = useState({ value: '', valid: false });
  const [lastName, setLastName] = useState({ value: '', valid: false });
  const [password, setPassword] = useState({ value: '', valid: false });
  const [success, setSuccess] = useState(false);
  const [btnState, setBtnState] = useState({
    text: 'buttons.signUp',
    defaultText: 'buttons.signUp',
    disabled: false,
  });

  const router = useRouter();

  const { locale } = router;

  const { toggleAlert } = useContext(PageContext);

  const cookie = useCookie();

  let userLanguageId = locales.find((item) => item.slug === locale)?.id || 2;

  if (userLanguageId === 4) userLanguageId = 2;

  useEffect(() => {
    const localEmail = localStorage.getItem('email');

    if (localEmail) {
      setEmail({ value: localEmail, valid: true });
      localStorage.removeItem('email');
    }
  }, [email, setEmail]);

  useEffect(() => {
    let { referralCode } = router.query;

    if (referralCode) cookie.set('referralCode', referralCode, '/', '', 3600 * 24 * 30);
    else referralCode = cookie.get('referralCode');

    setReferralCode(referralCode);
  }, [router, cookie]);

  const getDialCodeId = async (dialCode) => {
    const response = await axios.get('/codes.json');

    const targetCodeKey = Object.keys(response.data).find(
      (item) => String(item).replace(/\D/, '') === dialCode
    );

    if (!targetCodeKey) return Promise.reject(new Error('Dial code not found'));

    return targetCodeKey;
  };

  const logIn = () => {
    axios
      .post(
        api.login,
        {
          email: email.value,
          password: password.value,
        },
        {
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )
      .then((response) => {
        const { accessToken, refreshToken } = response.data;

        cookie.set('accessToken', accessToken, '/', '', 3600 * 24 * 30);
        cookie.set('refreshToken', refreshToken, '/', '', 3600 * 24 * 30);
      })
      .catch((response) => {});
  };

  const cleanUp = () => {
    cookie.remove('referralCode');

    setSuccess(true);
    setEmail({ value: '', valid: false });
    setPhone({ value: '', dialCode: '', valid: false });
    setFirstName({ value: '', valid: false });
    setLastName({ value: '', valid: false });
    setPassword({ value: '', valid: false });
    setBtnState({ ...btnState, text: btnState.defaultText, disabled: false });
  };

  const signUp = () => {
    setBtnState({ ...btnState, text: 'status.loading', disabled: true });

    getDialCodeId(phone.dialCode)
      .then((dialCodeId) =>
        axios
          .post(
            api.signUp,
            {
              email: email.value,
              firstName: firstName.value,
              lastName: lastName.value,
              password: password.value,
              phoneNumber: phone.value.slice(phone.dialCode.length),
              valueDialInCode: dialCodeId,
              idUserLanguage: userLanguageId,
              referralCode,
            },
            {
              headers: {
                'Content-Type': 'application/json',
              },
            }
          )
          .then(({ status }) => status === 201 && logIn())
          .then(() => cleanUp())
          .catch(({ response }) => {
            toggleButtonState('status.error');
            toggleAlert({
              text: response.data?.error || _('status.somethingWrong'),
              type: 'error',
              duration: 5000,
            });
          })
      )
      .catch((error) => {
        toggleButtonState('status.error');
        toggleAlert({ text: error.message, type: 'error' });
      });
  };

  const btnClick = () => {
    if ([email, firstName, lastName, password].every((item) => item.valid)) signUp();
  };

  const toggleButtonState = (text) => {
    setBtnState({ ...btnState, text, disabled: true });

    setTimeout(() => {
      setBtnState({ ...btnState, text: btnState.defaultText, disabled: false });
    }, 2000);
  };

  const fields = [
    {
      id: 1,
      props: {
        name: 'firstName',
        placeholderText: _('fields.firstName.placeholder'),
        icon: 'user-card',
        value: firstName.value,
        validate: validateName,
        onChange: (e, isValid) => {
          setFirstName({ value: e.target.value, valid: isValid });
        },
      },
    },
    {
      id: 2,
      props: {
        name: 'lastName',
        placeholderText: _('fields.lastName.placeholder'),
        icon: 'user-card',
        value: lastName.value,
        validate: validateName,
        onChange: (e, isValid) => {
          setLastName({ value: e.target.value, valid: isValid });
        },
      },
    },
    {
      id: 3,
      props: {
        name: 'phone',
        country: _('fields.phone.country'),
        detectLocation: true,
        placeholderText: _('fields.phone.placeholder'),
        value: phone.value,
        validate: (value) => {
          return {
            isValid: value.length > 7,
            message: _('fields.phone.validation'),
          };
        },
        onChange: (value, data, e, formattedValue, isValid) => {
          setPhone({
            value: value,
            dialCode: data.dialCode,
            valid: isValid,
          });
        },
      },
    },
    {
      id: 4,
      props: {
        name: 'email',
        placeholderText: _('fields.email.placeholder'),
        type: 'email',
        icon: 'email',
        value: email.value,
        validate: (value) => {
          return {
            isValid: !!value.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/),
            message: _('fields.email.validation'),
          };
        },
        onChange: (e, isValid) => {
          setEmail({ value: e.target.value, valid: isValid });
        },
      },
    },
    {
      id: 5,
      props: {
        name: 'password',
        type: 'password',
        placeholderText: _('fields.password.placeholder'),
        icon: 'key',
        value: password.value,
        validate: (value) => {
          return {
            isValid: 6 <= value.length && value.length <= 16,
            message: _('fields.password.validation'),
          };
        },
        onChange: (e, isValid) => {
          setPassword({ value: e.target.value, valid: isValid });
        },
      },
    },
  ];

  return (
    <div className={styles.formWrapper}>
      <div className={cn(styles.form, success ? 'growOutAnimated' : 'growInAnimated')}>
        {fields.map((field) =>
          field.props.name === 'phone' ? (
            <PhoneInput key={field.id} className={styles[field.props.name]} {...field.props} />
          ) : (
            <LabelInput key={field.id} className={styles[field.props.name]} {...field.props} />
          )
        )}
        <Button
          className={styles.btn}
          type="primary"
          disabled={btnState.disabled}
          onClick={btnClick}
        >
          {_(btnState.text)}
        </Button>
      </div>
      <WelcomeBanner active={success} />
    </div>
  );
};
