import classNames from 'classnames';
import styles from './banners.module.scss';

import { _ } from 'translator';

const WelcomeBanner = ({ active }) => {
  return (
    <div className={classNames(styles.banner, active ? 'growInAnimated' : 'growOutAnimated')}>
      <p className={styles.title}>{_('signUpPage.welcomeBanner.welcome')}</p>
      <p
        className={styles.text}
        dangerouslySetInnerHTML={{
          __html: _('signUpPage.welcomeBanner.checkEmailBox'),
        }}
      ></p>
      <p className={classNames(styles.bottomText, styles.bottomTextEn)}>
        {_('signUpPage.welcomeBanner.feelFree.line1')}{' '}
        <a href="mailto:support@just-link-it.com">support@just-link-it.com</a>{' '}
        {_('signUpPage.welcomeBanner.feelFree.line2')} <br />
        <br />
        {_('signUpPage.welcomeBanner.checkSpam')}
      </p>
    </div>
  );
};

export default WelcomeBanner;
