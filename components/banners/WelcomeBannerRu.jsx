import classNames from 'classnames';
import styles from './banners.module.scss';

import { telegramChannel } from 'const';

const WelcomeBannerRu = ({ active }) => {
  return (
    <div className={classNames(styles.banner, active ? 'growInAnimated' : 'growOutAnimated')}>
      <p className={styles.title}>Привет и добро пожаловать!</p>
      <p>
        Поздравляем, вы успешно зарегистрировались на Just-Link-It.com. Осталось всего несколько
        шагов и мы будем готовы добиваться совместных целей! <br />
        <br />
        На вашу почту было отправлено письмо с обучающим руководством и дополнительной информацией.
      </p>
      {/* <div className={styles.tip}>
        <p>Также вы можете присоединиться к нашему</p>
        <a href={telegramChannel} className={styles.link} target="_blank" rel="noreferrer">
          <i className="icon-telegram"></i>Телеграмм-каналу
        </a>
        <p>
          чтобы получить доступ к базе знаний, узнать об обновлениях в системе или задать вопросы
        </p>
      </div> */}
      <p className={styles.bottomText}>
        Если у вас остались вопросы, свяжитесь с нами по адресу{' '}
        <a href="mailto:support@just-link-it.com">support@just-link-it.com</a>
        <br />
        P.S. Если вы не можете найти письмо, пожалуйста, проверьте папку “Спам”.
      </p>
    </div>
  );
};

export default WelcomeBannerRu;
