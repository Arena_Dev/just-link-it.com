import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import styles from './SectionGetCall.module.scss';

import { _ } from 'translator';
import GetCallForm from 'components/forms/GetCallForm';

export const SectionGetCall = () => {
  return (
    <section className={styles.section}>
      <Container className={styles.wrapper}>
        <div className={styles.icon}>
          <ResponsiveImage
            alt="Icon"
            width={94}
            height={91}
            src="/img/shared/customer-service.png"
            src2x="/img/shared/customer-service@2x.png"
          />
        </div>
        <div className={styles.content}>
          <div className={styles.text}>
            <strong>{_('contactsPage.sectionGetCall.title')}</strong>
            <p>{_('contactsPage.sectionGetCall.text')}</p>
          </div>
          <GetCallForm className={styles.form} />
        </div>
      </Container>
    </section>
  );
};
