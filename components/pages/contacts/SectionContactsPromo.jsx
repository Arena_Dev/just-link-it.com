import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import styles from './SectionContactsPromo.module.scss';
import { _ } from 'translator';
import { ContactForm } from 'components/forms/ContactForm';

export const SectionContactsPromo = () => (
  <section className={styles.section}>
    <Container className={styles.wrapper}>
      <div className={styles.content}>
        <h1
          className={styles.heading}
          dangerouslySetInnerHTML={{
            __html: _('contactsPage.sectionForm.heading'),
          }}
        />
        <ContactForm />
      </div>
      <div className={styles.heroImage}>
        <ResponsiveImage
          className={styles.bubbles}
          alt="Message bubbles"
          width={470}
          height={277}
          src="/img/pages/contact-us/3d-speech-bubbles.png"
          src2x="/img/pages/contact-us/3d-speech-bubbles@2x.png"
        />
        <ResponsiveImage
          className={styles.girl}
          alt="Girl on chair"
          width={470}
          height={460}
          src="/img/pages/contact-us/girl-speaking-on-chair.png"
          src2x="/img/pages/contact-us/girl-speaking-on-chair@2x.png"
        />
      </div>
    </Container>
  </section>
);
