import Link from 'next/link';
import { useState } from 'react';
import { Container } from 'components/layout/Container';
import { ContentsTable } from 'components/layout/legal/ContentsTable';
import { Content } from 'components/UI/Typography/Content';
import styles from './SectionFaqContent.module.scss';

import { _ } from 'translator';
import { CollapseItem } from 'components/shared/CollapseItem';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

const images = [
  {
    src: '/img/pages/faq/hiring.png',
    src2x: '/img/pages/faq/hiring@2x.png',
    width: 102,
    height: 88,
  },
  {
    src: '/img/pages/faq/schedule.png',
    src2x: '/img/pages/faq/schedule@2x.png',
    width: 95,
    height: 87,
  },
  {
    src: '/img/pages/faq/collaboration.png',
    src2x: '/img/pages/faq/collaboration@2x.png',
    width: 85,
    height: 83,
  },
  {
    src: '/img/pages/faq/payments.png',
    src2x: '/img/pages/faq/payments@2x.png',
    width: 74,
    height: 101,
  },
];

const SideLink = ({ icon, title, href, key }) => (
  <div key={key} className={styles.sideLink}>
    <Link href={href}>
      <a>
        <div className={styles.sideLinkIcon}>
          <i className={icon}></i>
        </div>
        {title}
        <i className="icon-chevron-right" />
      </a>
    </Link>
  </div>
);

const AccordionItem = ({ question, answer }) => {
  const [expanded, setExpanded] = useState(false);
  return (
    <CollapseItem
      type="grouped"
      expanded={expanded}
      onToggle={(expanded) => {
        setExpanded(!expanded);
      }}
      title={question}
    >
      {answer}
    </CollapseItem>
  );
};

export const SectionFaqContent = ({ faq }) => {
  const sideLinks = [
    {
      key: 2,
      title: _('nav.contactUs'),
      href: '/contact-us',
      icon: 'icon-headphones-round',
    },
  ];

  return (
    <Container className={styles.container}>
      <div className={styles.contentsTableWrap}>
        <ContentsTable items={faq} />
        <div className={styles.sideButtons}>{sideLinks.map(SideLink)}</div>
      </div>
      <Content>
        <div className={styles.contentButtons}>{sideLinks.map(SideLink)}</div>
        <main className={styles.main}>
          {_('faqPage.faq', { returnObjects: true }).map(({ id, heading, content }, index) => (
            <div key={id} className={styles.faqWrap}>
              <div id={id} />
              <ResponsiveImage {...images[index]} alt={id} className={styles.image} />
              <h2 className={styles.heading}>{heading}</h2>
              <div className={styles.collapseGroup}>
                {content.map(({ question, answer }, index) => (
                  <AccordionItem key={index} question={question} answer={answer} />
                ))}
              </div>
            </div>
          ))}
        </main>
      </Content>
    </Container>
  );
};
