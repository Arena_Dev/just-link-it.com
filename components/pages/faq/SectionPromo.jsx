import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import styles from './SectionPromo.module.scss';

const SectionPromo = ({ heading }) => {
  return (
    <section className={styles.section}>
      <Container className={styles.wrapper}>
        <h1
          className={styles.heading}
          dangerouslySetInnerHTML={{
            __html: heading,
          }}
        />
        <div className={styles.heroImg}>
          <ResponsiveImage
            className={styles.image}
            alt="faq hero image"
            width={485}
            height={368}
            src="/img/pages/faq/hero.png"
            src2x="/img/pages/faq/hero@2x.png"
          />
        </div>
      </Container>
    </section>
  );
};

export default SectionPromo;
