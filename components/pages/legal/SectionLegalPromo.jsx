import { Container } from 'components/layout/Container';
import styles from './SectionLegalPromo.module.scss';

import { _ } from 'translator';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

export const SectionLegalPromo = ({ image, heading, releaseDate }) => {
  return (
    <section className={styles.section}>
      <Container>
        <ResponsiveImage alt={heading} className={styles.image} {...image} />
        <h1
          className={styles.heading}
          dangerouslySetInnerHTML={{
            __html: heading,
          }}
        />
        <p className={styles.data}>
          {_('shared.effectiveAs')} {releaseDate}
        </p>
      </Container>
    </section>
  );
};
