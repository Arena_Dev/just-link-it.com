import { Container } from 'components/layout/Container';
import { ContentsTable } from 'components/layout/legal/ContentsTable';
import { LegalItem } from 'components/layout/legal/LegalItem';
import { Content } from 'components/UI/Typography/Content';
import styles from './SectionLegalContent.module.scss';

export const SectionLegalContent = ({ items }) => {
  return (
    <Container className={styles.container}>
      <div className={styles.contentsTableWrap}>
        <ContentsTable items={items} />
      </div>
      <Content>
        <main className={styles.main}>
          {items.map((item) => (
            <LegalItem className={styles.list} key={item.id} itemId={item.id} {...item} />
          ))}
        </main>
      </Content>
    </Container>
  );
};
