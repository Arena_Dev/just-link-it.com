import { Container } from 'components/layout/Container';
import styles from './SectionCollaborationPromo.module.scss';

import { _ } from 'translator';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

export const SectionCollaborationPromo = ({ image, heading, releaseDate }) => {
  return (
    <section className={styles.section}>
      <Container className={styles.inner}>
        <div className={styles.content}>
          <h1
            className={styles.heading}
            dangerouslySetInnerHTML={{
              __html: heading,
            }}
          />
          <p className={styles.data}>
            {_('shared.effectiveAs')} {releaseDate}
          </p>
        </div>

        <div className={styles.imageWrap}>
          <ResponsiveImage alt={heading} className={styles.image} {...image} />
        </div>
      </Container>
    </section>
  );
};
