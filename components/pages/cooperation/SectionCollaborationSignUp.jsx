import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { SignUpForm } from 'components/forms/SignUpForm';
import { _ } from 'translator';
import styles from './SectionCollaborationSignUp.module.scss';

export const SectionCollaborationSignUp = () => (
  <section className={styles.section}>
    <Container className={styles.container}>
      <div className={styles.left}>
        <h2
          className={styles.heading}
          dangerouslySetInnerHTML={{ __html: _('cooperationPage.sectionSignUp.heading') }}
        />
        <ul className={styles.list}>
          {_('cooperationPage.sectionSignUp.list', { returnObjects: true }).map((item, index) => (
            <li key={index}>
              {item}{' '}
              {!index && (
                <div className={styles.walletWrap}>
                  <ResponsiveImage
                    src="/img/pages/collaboration/wallet.png"
                    src2x="/img/pages/collaboration/wallet@2x.png"
                    width={47}
                    height={46}
                  />
                  <span>$500+</span>
                </div>
              )}
            </li>
          ))}
        </ul>
        <SignUpForm />
      </div>
      <div className={styles.right}>
        <ResponsiveImage
          src="/img/pages/collaboration/sign-up.png"
          src2x="/img/pages/collaboration/sign-up@2x.png"
          alt="Sign up"
          width={617}
          height={600}
          className={styles.image}
        />
      </div>
    </Container>
    <ResponsiveImage
      src="/img/pages/collaboration/background.svg"
      layout="fill"
      objectPosition="bottom center"
      className={styles.bg}
    />
  </section>
);
