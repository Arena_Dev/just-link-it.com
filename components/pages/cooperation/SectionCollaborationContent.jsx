import { Container } from 'components/layout/Container';
import { ContentsTable } from 'components/layout/legal/ContentsTable';
import { LegalItem } from 'components/layout/legal/LegalItem';
import { Content } from 'components/UI/Typography/Content';
import styles from './SectionCollaborationContent.module.scss';
import { _ } from 'translator';
import Link from 'next/link';

const SideLink = ({ icon, title, href, key }) => (
  <div key={key} className={styles.sideLink}>
    <Link href={href}>
      <a>
        <div className={styles.sideLinkIcon}>
          <i className={icon}></i>
        </div>
        {title}
        <i className="icon-chevron-right" />
      </a>
    </Link>
  </div>
);

const images = [
  {
    src: '/img/pages/collaboration/book.png',
    src2x: '/img/pages/collaboration/book@2x.png',
    width: 122,
    height: 91,
  },
  {
    src: '/img/pages/collaboration/pin.png',
    src2x: '/img/pages/collaboration/pin@2x.png',
    width: 84,
    height: 54,
  },
  {
    src: '/img/pages/collaboration/coin.png',
    src2x: '/img/pages/collaboration/coin@2x.png',
    width: 68,
    height: 68,
  },
];

export const SectionCollaborationContent = () => {
  const sideLinks = [
    {
      key: 1,
      title: _('nav.faqFull'),
      href: '/faq',
      icon: 'icon-question',
    },
    {
      key: 2,
      title: _('nav.contactUs'),
      href: '/contact-us',
      icon: 'icon-headphones-round',
    },
  ];

  const items = _('cooperationPage.sectionContent.items', { returnObjects: true });

  return (
    <Container className={styles.container}>
      <div className={styles.contentsTableWrap}>
        <ContentsTable items={items} />
        <div className={styles.sideButtons}>{sideLinks.map(SideLink)}</div>
      </div>
      <Content>
        <main className={styles.main}>
          <div
            className={styles.beforeContent}
            dangerouslySetInnerHTML={{
              __html: _('cooperationPage.sectionContent.beforeContent'),
            }}
          />
          {items.map((item, index) => (
            <LegalItem
              className={styles.list}
              key={item.id}
              itemId={item.id}
              {...item}
              image={{
                ...images[index + 1],
                alt: item.heading,
              }}
            />
          ))}
        </main>
      </Content>
    </Container>
  );
};
