import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { _ } from 'translator';
import styles from './SectionCourse.module.scss';

const images = [
  {
    width: 89,
    height: 36,
  },
  {
    width: 80,
    height: 94,
  },
  {
    width: 68,
    height: 55,
  },
  {
    width: 55,
    height: 53,
  },
  {
    width: 69,
    height: 61,
  },
];

export const SectionCourse = () => (
  <section className={styles.section}>
    <Container className={styles.container}>
      <div className={styles.content}>
        <h2
          className={styles.heading}
          dangerouslySetInnerHTML={{
            __html: _('homepage.sectionCourse.heading'),
          }}
        />
        <p className={styles.subheading}>{_('homepage.sectionCourse.subheading')}</p>
        <p className={styles.listTitle}>
          <strong>{_('homepage.sectionCourse.listTitle')}</strong>
        </p>
        <ul className={styles.list}>
          {_('homepage.sectionCourse.list', { returnObjects: true }).map((text, index) => (
            <li key={index}>{text}</li>
          ))}
        </ul>
      </div>
      <div className={styles.stages}>
        <div className={styles.stagesLogo}>
          <div className={styles.stagesLogoInner}>
            <ResponsiveImage
              src={`/img/logo-main.svg`}
              src2x={`/img/logo-main.svg`}
              width={39}
              height={73}
              alt="Just-link-it"
            />
          </div>
        </div>
        <ol className={styles.stagesList}>
          {_('homepage.sectionCourse.stages', { returnObjects: true }).map((text, index) => (
            <li key={index}>
              <ResponsiveImage
                src={`/img/pages/homepage/stage-${index + 1}.png`}
                src2x={`/img/pages/homepage/stage-${index + 1}@2x.png`}
                width={images[index].width}
                height={images[index].height}
                className={styles.img}
              />
              <span
                className={styles.stagesText}
                dangerouslySetInnerHTML={{
                  __html: text,
                }}
              />
            </li>
          ))}
        </ol>
      </div>
    </Container>
  </section>
);
