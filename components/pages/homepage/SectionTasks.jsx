import { Container } from 'components/layout/Container';
import styles from './SectionTasks.module.scss';
import { _ } from 'translator';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

export const SectionTasks = () => (
  <section className={styles.section}>
    <Container>
      <h2
        className={styles.heading}
        dangerouslySetInnerHTML={{ __html: _('homepage.sectionTasks.heading') }}
      />
      <div className={styles.wrapper}>
        <div className={styles.item}>
          <h3 className={styles.itemTitle}>{_('homepage.sectionTasks.search.heading')}</h3>
          <p className={styles.itemText}>{_('homepage.sectionTasks.search.subheading')}</p>
          <div className={styles.listWrap}>
            <div className={styles.buttons}></div>
            <ul className={styles.list}>
              {_('homepage.sectionTasks.search.list', { returnObjects: true }).map(
                (item, index) => (
                  <li key={index}>
                    <ResponsiveImage
                      src="/img/pages/homepage/icons/check.svg"
                      width={24}
                      height={24}
                      className={styles.icon}
                    />
                    {item}
                  </li>
                )
              )}
            </ul>
          </div>
        </div>
        <div className={styles.item}>
          <h3 className={styles.itemTitle}>{_('homepage.sectionTasks.emails.heading')}</h3>
          <p className={styles.itemText}>{_('homepage.sectionTasks.emails.subheading')}</p>
        </div>
        <div className={styles.item}>
          <h3 className={styles.itemTitle}>{_('homepage.sectionTasks.mentor.heading')}</h3>
          <p className={styles.itemText}>{_('homepage.sectionTasks.mentor.subheading')}</p>
        </div>
      </div>
    </Container>
  </section>
);
