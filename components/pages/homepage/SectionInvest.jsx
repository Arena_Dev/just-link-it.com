import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { _ } from 'translator';
import styles from './SectionInvest.module.scss';

export const SectionInvest = () => (
  <section className={styles.section}>
    <Container>
      <h2
        className={styles.heading}
        dangerouslySetInnerHTML={{ __html: _('homepage.sectionInvest.heading') }}
      />
      <ul className={styles.list}>
        {Object.entries(_('homepage.sectionInvest.items', { returnObjects: true })).map(
          ([slug, text], index) => (
            <li key={slug}>
              <div className={styles.iconWrap}>
                <ResponsiveImage
                  src={`/img/pages/homepage/icons/${slug}.svg`}
                  width={28}
                  height={28}
                  className={styles.icon}
                />
              </div>

              {text}
            </li>
          )
        )}
      </ul>
    </Container>
  </section>
);
