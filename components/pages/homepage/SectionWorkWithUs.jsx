import styles from './SectionWorkWithUs.module.scss';
import { Container } from 'components/layout/Container';
import { _ } from 'translator';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

const icons = ['heart', 'award', 'account-clock', 'dollar-bill'];

export const SectionWorkWithUs = () => (
  <section className={styles.section}>
    <Container>
      <h2
        className={styles.heading}
        dangerouslySetInnerHTML={{
          __html: _('homepage.sectionWorkWithUs.heading'),
        }}
      />
      <ul className={styles.list}>
        {_('homepage.sectionWorkWithUs.items', { returnObjects: true }).map(
          ({ title, text }, index) => (
            <li key={index}>
              <ResponsiveImage
                src={`/img/pages/homepage/icons/${icons[index]}.svg`}
                width={28}
                height={28}
                alt={title}
                className={styles.image}
              />
              <strong>{title}</strong>
              <p>{text}</p>
            </li>
          )
        )}
      </ul>
    </Container>
  </section>
);
