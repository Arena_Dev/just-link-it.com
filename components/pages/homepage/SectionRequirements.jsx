import { Container } from 'components/layout/Container';
import styles from './SectionRequirements.module.scss';
import { _ } from 'translator';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';

export const SectionRequirements = () => (
  <section className={styles.section}>
    <Container>
      <h2
        dangerouslySetInnerHTML={{ __html: _('homepage.sectionRequirements.heading') }}
        className={styles.heading}
      />
      <ul className={styles.list}>
        {Object.entries(_('homepage.sectionRequirements.items', { returnObjects: true })).map(
          ([slug, text]) => (
            <li key={slug}>
              <ResponsiveImage
                src={`/img/pages/homepage/icons/${slug}.svg`}
                width={28}
                height={28}
                alt={slug}
                className={styles.image}
              />{' '}
              {text}
            </li>
          )
        )}
      </ul>
    </Container>
  </section>
);
