import gsap from 'gsap';
import ScrollTrigger from 'gsap/dist/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

export const animations = ({
  animationWrapper,
  rocket,
  cloudTop,
  cloudBottom,
  spiralFirst,
  spiralSecond,
}) => {
  const triggerSettings = {
    trigger: animationWrapper,
    // markers: true,
    start: 'center bottom',
    end: 'center bottom',
  };

  gsap.to(animationWrapper, {
    opacity: 1,
  });
  gsap.from(rocket, {
    duration: 1,
    rotation: -30,
    xPercent: 150,
    yPercent: 25,
    scrollTrigger: triggerSettings,
  });
  gsap.from(cloudTop, {
    delay: 0.4,
    autoAlpha: 0,
    rotate: '45deg',
    xPercent: 40,
    scrollTrigger: triggerSettings,
  });
  gsap.from(cloudBottom, {
    delay: 0.4,
    autoAlpha: 0,
    rotate: '-45deg',
    scrollTrigger: triggerSettings,
  });
  gsap.from(spiralFirst, {
    delay: 0.4,
    autoAlpha: 0,
    scale: 0.2,
    rotate: '-20deg',
    scrollTrigger: triggerSettings,
  });
  gsap.from(spiralSecond, {
    delay: 0.4,
    autoAlpha: 0,
    scale: 0.2,
    rotate: '-20deg',
    scrollTrigger: triggerSettings,
  });
};
