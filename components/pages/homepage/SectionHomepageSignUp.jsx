import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { SignUpForm } from 'components/forms/SignUpForm';
import { _ } from 'translator';
import styles from './SectionHomepageSignUp.module.scss';

export const SectionHomepageSignUp = () => (
  <section className={styles.section}>
    <Container className={styles.container}>
      <div className={styles.left}>
        <h2
          className={styles.heading}
          dangerouslySetInnerHTML={{ __html: _('homepage.sectionSignUp.heading') }}
        />
        <div className={styles.subheading}>
          <ResponsiveImage
            src="/img/shared/hand.png"
            src2x="/img/shared/hand@2x.png"
            width={71}
            height={59}
          />
          {_('homepage.sectionSignUp.subheading')}
        </div>
        <SignUpForm />
      </div>
      <div className={styles.right}>
        <ResponsiveImage
          src="/img/shared/sing-up.png"
          src2x="/img/shared/sing-up@2x.png"
          alt="Sign up"
          width={516}
          height={601}
          className={styles.image}
        />
      </div>
    </Container>
  </section>
);
