import { Container } from 'components/layout/Container';
import { _ } from 'translator';
import styles from './SectionFindYourself.module.scss';

const DetailedListItem = ({ name, text }) => (
  <>
    <dt>{name}</dt>
    <dd>{text}</dd>
  </>
);

export const SectionFindYourself = () => (
  <section className={styles.section}>
    <Container>
      <h2
        className={styles.heading}
        dangerouslySetInnerHTML={{
          __html: _('homepage.sectionFindYourself.heading'),
        }}
      />
      <div className={styles.content}>
        <ul className={styles.list}>
          {_('homepage.sectionFindYourself.list', { returnObjects: true }).map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
        <dl className={styles.detailedList}>
          {_('homepage.sectionFindYourself.detailedList', { returnObjects: true }).map(
            ({ name, text }, index) => (
              <DetailedListItem key={index} name={name} text={text} />
            )
          )}
        </dl>
      </div>
    </Container>
  </section>
);
