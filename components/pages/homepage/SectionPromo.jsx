import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { useEffect, useRef, useState } from 'react';
import { animations } from './animations/SectionPromo';
import styles from './SectionPromo.module.scss';

import { _ } from 'translator';
import { Button } from 'components/UI/Buttons/Button';

const features = [
  {
    slug: 'schedule',
    image: 'clock',
    width: 53,
    height: 49,
  },
  {
    slug: 'remote',
    image: 'globe',
    width: 62,
    height: 62,
  },
  {
    slug: 'communication',
    image: 'dialog',
    width: 56,
    height: 37,
  },
];

export const SectionPromo = () => {
  const animationWrapper = useRef();
  const rocket = useRef();
  const cloudTop = useRef();
  const cloudBottom = useRef();
  const spiralFirst = useRef();
  const spiralSecond = useRef();

  const [imagesLoad, setImagesLoad] = useState({
    rocket: false,
    cloudTop: false,
    cloudBottom: false,
    spiralFirst: false,
    spiralSecond: false,
  });

  const [isAnimated, setIsAnimated] = useState(false);

  useEffect(() => {
    const layers = {
      animationWrapper: animationWrapper.current,
      rocket: rocket.current,
      cloudTop: cloudTop.current,
      cloudBottom: cloudBottom.current,
      spiralFirst: spiralFirst.current,
      spiralSecond: spiralSecond.current,
    };
    if (Object.values(imagesLoad).every((item) => item) && !isAnimated) {
      setIsAnimated(true);
      animations(layers);
    }
  }, [imagesLoad, isAnimated]);

  return (
    <section className={styles.section}>
      <Container>
        <div className={styles.top}>
          <div className={styles.content}>
            <div className={styles.beforeHeading}>
              <ResponsiveImage
                alt={_('homepage.sectionPromo.beforeHeading')}
                width={28}
                height={28}
                src="/img/pages/homepage/desktop.png"
                src2x="/img/pages/homepage/desktop@2x.png"
                className={styles.beforeHeadingImage}
              />{' '}
              {_('homepage.sectionPromo.beforeHeading')}
            </div>
            <h1
              className={styles.heading}
              dangerouslySetInnerHTML={{
                __html: _('homepage.sectionPromo.heading'),
              }}
            ></h1>
            <p className={styles.subheading}>{_('homepage.sectionPromo.subheading')}</p>
            <Button href="/sign-up" type="primary" className={styles.button}>
              {_('homepage.sectionPromo.btnText')}
            </Button>
          </div>

          <div ref={animationWrapper} className={styles.heroImgWrapper}>
            <div ref={rocket} className={styles.rocket}>
              <div className={styles.rocketWrap}>
                <ResponsiveImage
                  alt="Rocket"
                  layout="fill"
                  src="/img/pages/homepage/rocket.png"
                  src2x="/img/pages/homepage/rocket@2x.png"
                  onLoad={() => setImagesLoad({ ...imagesLoad, rocket: true })}
                />
              </div>
            </div>

            <div ref={cloudTop} className={styles.cloudTop}>
              <div className={styles.cloudTopWrap}>
                <ResponsiveImage
                  alt="Cloud"
                  layout="fill"
                  src="/img/pages/homepage/top-cloud.png"
                  src2x="/img/pages/homepage/top-cloud@2x.png"
                  onLoad={() => setImagesLoad({ ...imagesLoad, cloudTop: true })}
                />
              </div>
            </div>
            <div ref={cloudBottom} className={styles.cloudBottom}>
              <div className={styles.cloudTopWrap}>
                <ResponsiveImage
                  alt="Cloud"
                  layout="fill"
                  src="/img/pages/homepage/bottom-cloud.png"
                  src2x="/img/pages/homepage/bottom-cloud@2x.png"
                  onLoad={() => setImagesLoad({ ...imagesLoad, cloudBottom: true })}
                />
              </div>
            </div>
            <div ref={spiralFirst} className={styles.spiralFirst}>
              <ResponsiveImage
                alt="Spiral"
                layout="fill"
                src="/img/pages/homepage/spiral-1.png"
                src2x="/img/pages/homepage/spiral-1@2x.png"
                onLoad={() => setImagesLoad({ ...imagesLoad, spiralFirst: true })}
              />
            </div>

            <div ref={spiralSecond} className={styles.spiralSecond}>
              <ResponsiveImage
                alt="Spiral"
                layout="fill"
                src="/img/pages/homepage/spiral-2.png"
                src2x="/img/pages/homepage/spiral-2@2x.png"
                onLoad={() => setImagesLoad({ ...imagesLoad, spiralSecond: true })}
              />
            </div>
            <div className={styles.circles} />
          </div>
        </div>
        <ul className={styles.features}>
          {features.map(({ slug, image, width, height }) => (
            <li key={slug} className={styles.feature}>
              <ResponsiveImage
                alt={slug}
                width={width}
                height={height}
                src={`/img/pages/homepage/${image}.png`}
                src2x={`/img/pages/homepage/${image}@2x.png`}
                className={styles.featureImage}
              />
              <p
                dangerouslySetInnerHTML={{ __html: _(`homepage.sectionPromo.features.${slug}`) }}
              />
            </li>
          ))}
        </ul>
      </Container>
    </section>
  );
};
