import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import styles from './SectionSignUpPromo.module.scss';
import { _ } from 'translator';
import { SignUpForm } from 'components/forms/SignUpForm';

export const SectionSignUpPromo = () => (
  <section className={styles.section}>
    <Container className={styles.wrapper}>
      <div className={styles.content}>
        <h1
          className={styles.heading}
          dangerouslySetInnerHTML={{ __html: _('signUpPage.sectionForm.heading') }}
        />
        <div className={styles.text}>
          <ResponsiveImage
            src="/img/shared/hand.png"
            src2x="/img/shared/hand@2x.png"
            width={71}
            height={59}
          />
          <p>{_('signUpPage.sectionForm.subheading')}</p>
        </div>
      </div>

      <ResponsiveImage
        className={styles.heroImg}
        alt="man with laptop"
        width={516}
        height={601}
        src="/img/shared/sing-up.png"
        src2x="/img/shared/sing-up@2x.png"
      />

      <SignUpForm />
    </Container>

    <ResponsiveImage
      className={styles.fill}
      alt="section fill"
      width={1369}
      height={780}
      layout="fixed"
      src="/img/pages/sign-up/page-form-section-fill.svg"
    />
  </section>
);
