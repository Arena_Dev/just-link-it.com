import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { _ } from 'translator';
import styles from './SectionAboutUsPromo.module.scss';

export const SectionAboutUsPromo = () => (
  <section className={styles.section}>
    <Container>
      <ResponsiveImage
        src="/img/pages/about-us/promo.png"
        src2x="/img/pages/about-us/promo@2x.png"
        alt="Rocket"
        width={120}
        height={120}
        className={styles.image}
      />
      <h2
        className={styles.heading}
        dangerouslySetInnerHTML={{ __html: _('aboutUsPage.sectionPromo.heading') }}
      />
      <p
        className={styles.subheading}
        dangerouslySetInnerHTML={{ __html: _('aboutUsPage.sectionPromo.subheading') }}
      />
    </Container>
  </section>
);
