import { Container } from 'components/layout/Container';
import { _ } from 'translator';
import styles from './SectionAboutUsAbout.module.scss';

export const SectionAboutUsAbout = () => (
  <div className={styles.wrapper}>
    <Container className={styles.container}>
      {_('aboutUsPage.sectionAbout.sections', { returnObjects: true }).map(
        ({ heading, subheading }, index) => (
          <section key={index} className={styles.section}>
            <section className={styles.section}>
              <h2 className={styles.heading}>{heading}</h2>
              <p className={styles.subheading}>{subheading}</p>
            </section>
          </section>
        )
      )}
    </Container>
  </div>
);
