import { Container } from 'components/layout/Container';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { SignUpForm } from 'components/forms/SignUpForm';
import { Button } from 'components/UI/Buttons/Button';
import { _ } from 'translator';
import styles from './SectionAboutUsSignUp.module.scss';

export const SectionAboutUsSignUp = () => (
  <section className={styles.section}>
    <Container className={styles.container}>
      <div className={styles.left}>
        <h2
          className={styles.heading}
          dangerouslySetInnerHTML={{ __html: _('aboutUsPage.sectionSignUp.heading') }}
        />
        <div className={styles.subheading}>
          <ResponsiveImage
            src="/img/shared/hand.png"
            src2x="/img/shared/hand@2x.png"
            width={71}
            height={59}
          />
          {_('aboutUsPage.sectionSignUp.subheading')}
        </div>
        <SignUpForm />
      </div>
      <div className={styles.right}>
        <div className={styles.imageWrap}>
          <ResponsiveImage
            src="/img/pages/about-us/questions.png"
            src2x="/img/pages/about-us/questions@2x.png"
            alt="Sign up"
            width={342}
            height={287}
            className={styles.image}
          />
        </div>
        <p
          className={styles.questions}
          dangerouslySetInnerHTML={{
            __html: _('aboutUsPage.sectionSignUp.anyQuestions'),
          }}
        />
        <Button type="primaryOutline" href="https://t.me/HRJustLinkIt" className={styles.button}>
          {_('aboutUsPage.sectionSignUp.consultMe')}
        </Button>
      </div>
    </Container>
  </section>
);
