import { useRef, useState } from 'react';
import cn from 'classnames';
import { Container } from 'components/layout/Container';
import { _ } from 'translator';
import styles from './SectionAboutUsItems.module.scss';
import { ResponsiveImage } from 'components/shared/ResponsiveImage';
import { useEventListener } from 'helpers/useEventListener';
import { gsap } from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';

gsap.registerPlugin(ScrollToPlugin);

export const SectionAboutUsItems = () => {
  const [isSticky, setSticky] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);
  const container = useRef();
  const itemsWrap = useRef();

  const handleScroll = () => {
    if (!container.current) return;
    setSticky(container.current.getBoundingClientRect().top - 160 < 0);
    if (!itemsWrap.current) return;

    const activeItemIndex =
      itemsWrap.current.children.length -
      1 -
      [...itemsWrap.current.children]
        .reverse()
        .findIndex((item) => item.getBoundingClientRect().top - 160 < 0);
    setActiveIndex(activeItemIndex === itemsWrap.current.children.length ? 0 : activeItemIndex);
  };

  const scrollToIndex = (index) => {
    console.log(itemsWrap.current.children[index].offsetTop);
    gsap.to(window, 0.25, {
      scrollTo: {
        y: itemsWrap.current.children[index].getBoundingClientRect().top + window.scrollY - 150,
      },
    });
  };

  useEventListener('scroll', handleScroll);

  const items = _('aboutUsPage.sectionHowItWorks.items', { returnObjects: true });
  return (
    <section className={styles.section}>
      <Container>
        <h2
          className={styles.heading}
          dangerouslySetInnerHTML={{ __html: _('aboutUsPage.sectionHowItWorks.heading') }}
        />
      </Container>
      <div className={styles.itemsWrap} ref={container}>
        <div
          className={cn(styles.navWrap, {
            [styles.navSticky]: isSticky,
          })}
        >
          <Container>
            <ol className={styles.nav}>
              {items.map(({ topTitle }, index) => (
                <li
                  key={index}
                  className={cn({
                    [styles.active]: index <= activeIndex,
                  })}
                  onClick={() => scrollToIndex(index)}
                >
                  <span dangerouslySetInnerHTML={{ __html: topTitle }} />
                </li>
              ))}
            </ol>
          </Container>
        </div>

        <Container>
          <div className={styles.main} ref={itemsWrap}>
            {items.map(({ title, text }, index) => (
              <div key={index} className={styles.item}>
                <ResponsiveImage
                  src={`/img/pages/about-us/stages/${index + 1}.png`}
                  src2x={`/img/pages/about-us/stages/${index + 1}@2x.png`}
                  width={400}
                  height={283}
                  alt={title}
                  className={styles.img}
                />
                <div className={styles.content}>
                  <strong>{title}</strong>
                  <p>{text}</p>
                </div>
              </div>
            ))}
          </div>
        </Container>
      </div>
    </section>
  );
};
