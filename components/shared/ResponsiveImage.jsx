import Image from 'next/image';
import { useState } from 'react';
import useDimensions from 'react-cool-dimensions';

export const ResponsiveImage = ({
  src,
  src2x,
  srcTab,
  srcMob,
  className,
  alt,
  title,
  objectFit = 'contain',
  objectPosition,
  width,
  height,
  layout,
  quality = 100,
  imageClassName,
  onLoad,
  breakpoints = {
    xs: 0,
    sm: 576,
    md: 920,
  },
  relative,
}) => {
  const [imgSrc, setImgSrc] = useState(src);

  const { observe } = useDimensions({
    breakpoints,
    onResize: ({ currentBreakpoint }) => {
      if (currentBreakpoint === 'xs' && (srcMob || srcTab)) {
        setImgSrc(srcMob || srcTab);
      } else if (currentBreakpoint === 'sm' && srcTab) {
        setImgSrc(srcTab);
      } else if (window.devicePixelRatio > 1 && src2x) {
        setImgSrc(src2x);
      } else {
        setImgSrc(src);
      }
    },
  });

  const style = {
    display: 'inline-block',
  };

  return (
    <div
      className={className}
      ref={observe}
      style={relative ? { ...style, position: 'relative' } : style}
    >
      <Image
        src={imgSrc}
        width={width}
        height={height}
        layout={layout}
        quality={quality}
        objectFit={objectFit}
        objectPosition={objectPosition}
        // unoptimized={true}
        alt={alt}
        title={title}
        className={imageClassName}
        onLoad={onLoad}
      />
    </div>
  );
};
