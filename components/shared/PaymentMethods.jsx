import styles from './PaymentMethods.module.scss';
import { ResponsiveImage } from './ResponsiveImage';

export const PaymentMethods = () => {
  return (
    <ul className={styles.payMethods}>
      <li>
        <ResponsiveImage
          title="PayPal icon"
          alt="PayPal"
          width={64}
          height={16}
          src="/img/shared/payment-systems/paypal.png"
          src2x="/img/shared/payment-systems/paypal@2x.png"
        />
      </li>
      <li>
        <ResponsiveImage
          title="Visa icon"
          alt="Visa"
          width={50}
          height={16}
          src="/img/shared/payment-systems/visa.png"
          src2x="/img/shared/payment-systems/visa@2x.png"
        />
      </li>
      <li>
        <ResponsiveImage
          title="Mastercard icon"
          alt="Mastercard"
          width={25}
          height={16}
          src="/img/shared/payment-systems/mastercard.png"
          src2x="/img/shared/payment-systems/mastercard@2x.png"
        />
      </li>
      <li>
        <ResponsiveImage
          title="Payoneer icon"
          alt="Payoneer"
          width={83}
          height={16}
          src="/img/shared/payment-systems/payoneer.png"
          src2x="/img/shared/payment-systems/payoneer@2x.png"
        />
      </li>
    </ul>
  );
};
