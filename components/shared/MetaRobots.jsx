import { isStage } from 'const';
import Head from 'next/head';
import { useRouter } from 'next/router';

export const MetaRobots = () => {
  const router = useRouter();
  const currentPagePath = router.asPath;

  const excludeList = [];

  const isPageExclude = excludeList.includes(currentPagePath);

  return (
    <Head>
      {isStage || isPageExclude ? (
        <meta name="robots" content="noindex, nofollow" />
      ) : (
        <meta name="robots" content="index, follow" />
      )}
    </Head>
  );
};
