import Link from 'next/link';
import classNames from 'classnames';
import LogoSvg from 'public/img/logo.svg';

import styles from './Logo.module.scss';

export const Logo = ({ className }) => {
  return (
    <Link href="/">
      <a className={classNames(styles.logo, className)}>
        <LogoSvg />
      </a>
    </Link>
  );
};
