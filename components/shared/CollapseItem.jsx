import classNames from 'classnames';
import { useEffect, useRef } from 'react';
import styles from './CollapseItem.module.scss';

export const CollapseItem = ({ title, number, type, expanded, onToggle, children, className }) => {
  const textWrapper = useRef();

  useEffect(() => {
    if (expanded) textWrapper.current.style.maxHeight = textWrapper.current.scrollHeight + 'px';
    else textWrapper.current.style.maxHeight = 0;
  }, [expanded]);

  const handleClick = (e) => {
    if (onToggle) onToggle(expanded, e);
  };

  return (
    <div
      className={classNames(styles.item, styles[type], className, expanded && styles.expanded)}
      onClick={handleClick}
    >
      <div className={styles.head}>
        <strong>
          {number && <span>{number}</span>}
          {title}
        </strong>
        <button>
          <i className="icon-chevron-down"></i>
        </button>
      </div>
      <div ref={textWrapper} className={styles.textWrapper}>
        <div className={styles.text}>{children}</div>
      </div>
    </div>
  );
};
