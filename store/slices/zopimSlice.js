import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  initialized: false,
  storageChecked: false,
};

export const zopimSlice = createSlice({
  name: 'zopim',
  initialState,
  reducers: {
    initialize: (state, action) => {
      state.initialized = action.payload;
    },
    checkStrorage: (state) => {
      state.storageChecked = true;
    },
  },
});

// Action creators are generated for each case reducer function
export const { initialize, checkStrorage } = zopimSlice.actions;

export default zopimSlice.reducer;

// Selectors
export const zopimInitSelector = (state) => state.zopim.initialized;
