import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  userInteracted: false,
  bodyOverflow: false,
};

export const globalSlice = createSlice({
  name: 'global',
  initialState,
  reducers: {
    setInteracted: (state) => {
      state.userInteracted = true;
    },
    setBodyOverflow(state, { payload }) {
      state.bodyOverflow = payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setInteracted, setBodyOverflow } = globalSlice.actions;

export default globalSlice.reducer;

// Selectors
export const userInteractedSelector = ({ global }) => global.userInteracted;
