import { configureStore } from '@reduxjs/toolkit';
import zopim from './slices/zopimSlice';

export const store = configureStore({
  reducer: {
    zopim,
  },
});
