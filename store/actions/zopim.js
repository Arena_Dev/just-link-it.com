import { zopim } from 'const';
import { initialize } from 'store/slices/zopimSlice';

const { id, color, siteName, logo } = zopim;

export const createScriptTag = (src, onLoad, onError) => {
  const script = document.createElement('script');
  script.async = true;
  script.setAttribute('charset', 'utf-8');
  script.id = 'ze-snippet';
  script.src = src;
  script.onload = onLoad;
  script.onerror = onError;

  return script;
};

const initZopimGlobal = (onLoad, onError) => {
  const z = (window.$zopim = function (c) {
    z._.push(c);
  });
  const script = (z.s = createScriptTag(
    'https://static.zdassets.com/ekr/snippet.js?key=' + id,
    onLoad,
    onError
  ));
  z.set = function (o) {
    z.set._.push(o);
  };
  z._ = [];
  z.set._ = [];
  z.t = +new Date();
  const firstScript = document.getElementsByTagName('script')[0];
  firstScript.parentNode.insertBefore(script, firstScript);
};

export const loadScript = (openWindow) => (dispatch) => {
  console.log('Zopim > initialization...');

  const onLoad = () => {
    window.$zopim(() => {
      const zopimColor = typeof color != 'undefined' ? color : '#ffffff';
      const zopimLogo = logo;

      // Check instance
      const chat = window.$zopim?.livechat;
      if (!chat) return;
      console.log('Zopim > Checked successfully');

      chat.concierge.setName(siteName);
      chat.concierge.setAvatar(zopimLogo);
      chat.window.setTitle('Customer Support');
      chat.theme.setColors({ primary: zopimColor });
      chat.theme.reload();

      localStorage.setItem('zopim_autoinit', true);
      dispatch(initialize(true));
      if (openWindow) chat.window.show(); // show onLoad
    });
  };

  const onError = () => console.error('Zopim > Failed...');

  if (!window.$zopim) initZopimGlobal(onLoad, onError);
};
