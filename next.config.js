module.exports = {
  reactStrictMode: true,
  i18n: {
    locales: ['ua'],
    defaultLocale: 'ua',
    localeDetection: false,
  },
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });

    return config;
  },
};
