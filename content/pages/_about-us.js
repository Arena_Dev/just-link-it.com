import { _ } from 'translator';

export const getContent = () => ({
  meta: {
    title: _('aboutUsPage.meta.title'),
    description: _('aboutUsPage.meta.description'),
  },
});

export default getContent;
