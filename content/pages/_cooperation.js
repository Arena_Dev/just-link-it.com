import { _ } from 'translator';

export const getContent = () => ({
  meta: {
    title: _('cooperationPage.meta.title'),
    description: _('cooperationPage.meta.description'),
  },
  heading: _('cooperationPage.heading'),
  releaseDate: _('cooperationPage.releaseDate'),
});

export default getContent;
