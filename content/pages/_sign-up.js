import { _ } from 'translator';

export const getContent = () => ({
  meta: {
    title: _('signUpPage.meta.title'),
    description: _('signUpPage.meta.description'),
  },
});

export default getContent;
