import { _ } from 'translator';

const getContent = () => ({
  meta: {
    title: _('termsPage.meta.title'),
    description: _('termsPage.meta.description'),
  },
  heading: _('termsPage.heading'),
  releaseDate: _('termsPage.releaseDate'),
  items: _('termsPage.items', { returnObjects: true }),
});

export default getContent;
