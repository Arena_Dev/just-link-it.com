import { _ } from 'translator';

const getContent = () => ({
  meta: {
    title: _('faqPage.meta.title'),
    description: _('faqPage.meta.description'),
  },
  heading: _('faqPage.heading'),
  faq: _('faqPage.faq', { returnObjects: true }),
});

export default getContent;
