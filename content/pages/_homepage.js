import { _ } from 'translator';

const getContent = () => ({
  meta: {
    title: _('homepage.meta.title'),
    description: _('homepage.meta.description'),
  },
});

export default getContent;
