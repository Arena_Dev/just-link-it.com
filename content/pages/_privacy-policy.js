import { _ } from 'translator';

export const getContent = () => ({
  meta: {
    title: _('policyPage.meta.title'),
    description: _('policyPage.meta.description'),
  },
  heading: _('policyPage.heading'),
  releaseDate: _('policyPage.releaseDate'),
  items: _('policyPage.items', { returnObjects: true }),
});

export default getContent;
