import { _ } from 'translator';

export const getContent = () => ({
  meta: {
    title: _('contactsPage.meta.title'),
    description: _('contactsPage.meta.description'),
  },
});

export default getContent;
