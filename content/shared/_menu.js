import { _ } from 'translator';

export const getMenu = () => [
  { href: '/about-us', title: _('nav.aboutUs') },
  { href: '/cooperation', title: _('nav.cooperation') },
  { href: '/contact-us', title: _('nav.contact') },
  { href: '/faq', title: _('nav.faq') },
];
