/* eslint-disable import/no-anonymous-default-export */
import homepage from './pages/_homepage';
import cooperation from './pages/_cooperation';
import contactsPage from './pages/_contact-us';
import aboutUsPage from './pages/_about-us';
import faqPage from './pages/_faq';
import termsPage from './pages/_terms-and-conditions';
import signUpPage from './pages/_sign-up';
import policyPage from './pages/_privacy-policy';

import { getMenu } from './shared/_menu';

export { getMenu };

export default {
  homepage,
  cooperation,
  contactsPage,
  faqPage,
  termsPage,
  policyPage,
  signUpPage,
  aboutUsPage,
};
