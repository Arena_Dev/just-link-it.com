import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
// import en from './translations/en.json';
// import ru from './translations/ru.json';
// import de from './translations/de.json';
import ua from './translations/ua.json';

i18n.use(initReactI18next).init({
  resources: {
    // en: { translation: en },
    // ru: { translation: ru },
    // de: { translation: de },
    ua: { translation: ua },
  },
  lng: 'ua',
  fallbackLng: 'ua',
  // debug: true,
});

export const translator = i18n;

export const _ = (args, params) => i18n.t(args, params);
