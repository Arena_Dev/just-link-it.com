import { remove } from 'lodash';
import { useEffect } from 'react';

export const useCookie = () => {
  // TODO: read about cookieStore

  return {
    get values() {
      return Object.fromEntries(document.cookie.split(/;\s*/).map((pair) => pair.split('=')));
    },
    get(name) {
      return this.values[name];
    },
    set(name, value, path = '/', domain = '', maxAge = 3600, expires) {
      // TODO: refactor
      const live = expires ? `expires=${expires}` : `max-age=${maxAge}`;

      let cookie = `${encodeURIComponent(name)}=${encodeURIComponent(
        value
      )}; path=${path}; ${live};`;

      if (domain) cookie += ` domain=${domain};`;

      document.cookie = cookie;

      return cookie;
    },
    remove(name) {
      document.cookie = `${encodeURIComponent(name)}=null; max-age=0`;
    },
  };
};
