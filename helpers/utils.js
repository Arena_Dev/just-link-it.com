import { _ } from 'translator';

export const validateName = (value) => {
  const checkLength = 2 <= value.length && value.length <= 45;
  const checkSpecialChars = !/[`!@#$%^&*()_+=\[\]{};:"\\|,<>\/?~\d]/.test(value);
  return {
    isValid: checkLength && checkSpecialChars,
    message: !checkLength ? _('fields.firstName.validation') : _('fields.noDigitsAndSpecChars'),
  };
};
